using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MediaStorageAPI.Models;

namespace MediaStorageAPI.Data
{
    public class SongContext : DbContext
    {
        public SongContext (DbContextOptions<SongContext> options)
            : base(options)
        {
        }

        public DbSet<MediaStorageAPI.Models.Song> Song { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=data.prattl.net;Initial Catalog=MediaService;Persist Security Info=True;User ID=sa;Password=XXXXX");
        }
    }
}
