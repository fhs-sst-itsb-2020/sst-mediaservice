# Media Service

Media Service that manages BLOB delivery. No Authentication required.

## Port Mapping
Port Mapping must use 
`docker run -d -p 80:TARGET_HTTP_PORT -p 443:TARGET_HTTPS_PORT` 
since the container uses Port **80** and **443** internally.

## Known Issues
- The Database sometimes returns errors especially after Startup. Just wait a few seconds and try a few times. I'm working on it.
- At the moment BLOB Data is stored as an Byte Array in the Database. This is very slow and should be exchanged by some Data System Management.
- The Database is hosted externally, since the previous solution via Docker Compose needed to much resources.

## Technical Background
This is basically a database wrapper. There are two classes (Song.cs & SongBase64.cs). This is needed since the music data is not stored as a string, but as binary data. **All API Requests take and return SongBase64 Objects.**

### Song Class

    int id;
    byte[] BlobData;

The Constructor of the Song Class takes an id and a BASE64 String. The BASE64 String gets converted to a Byte Array.

### SongBase64 Class

    int id;
    string BlobString;
The Constructor of the SongBase64 Class takes an id and a Byte Array. The Byte Array gets converted to a Base 64 String.

## API Overview

**Example Song Result**
    
    {"id":0,"blobString":"SUQzAwAAAAQzb1RJVDIAAAA..."}

### GET Requests:

> #### Get Song IDs
>     GET: /mediastorage/api/Songs  - Get IDs of all Songs (No BLOB) 
>
> Example Result:
>     [1,2,3]
> #### Get Specific Song by ID
> 
>     GET: /mediastorage/api/Songs/{id} 
> Example Result:
> 
>     {"id":2,"blobString":"SUQzAwAAAAQzb..."}

### POST Request:

>     POST: /mediastorage/api/Songs
> 
> Body: **SongBase64** Object with optional ID.
>
> Results: Song ID.

### PUT Request

> Not tested yet!
> 
>     PUT: /mediastorage/api/Songs/{id} Body: **SongBase64** Object with matching ID. 
>
> Results: Empty if OK / Bad Request / Not Found

### DELETE Request

        DELETE: /mediastorage/api/Songs/{id}
    Deletes Song with specific ID.

## Projectfolders and -files

### Controllers
The SongsController.cs consists of all the REST-Calls available for the Media Service. Look at the API Overview to get a detailed list of specific calls.

### Data
The SongContext.cs is a class for handling our song-objects. It directly interacts with a database through the .NET-EntityFrameworkCore.

### ExampleData
Just a song to test the application. You want to hear the song! It's good! Promised!

### Models
The Model-Folder consists of two classes. The Song.cs converts the song to a Byte array for Blob-Storage, where the SongBase64 is able to convert it back to a Base64-String.

### Program.cs
Starts the program itself. Uses Startup.cs to 

#### launchSettings.json 
Consists with the connections to launch the Project.
#### Other files
Just dependency files for mssql

### appsettings.Development.json
Information for Development-Configuration

### appsettings.json
File for connecting with Eureka.

### Dockerfile
Dockerfile for creating the Container with the .NET-Project

## Microservice Overview
![alt text](https://i.imgur.com/tPnk7sm.jpeg "Detail View")

### Fancy Stock Music Media Links

* https://sst-mediaservice.s3.eu-central-1.amazonaws.com/cool-saxophone.mp3
* https://sst-mediaservice.s3.eu-central-1.amazonaws.com/bensound-jazzyfrenchy.mp3
* https://sst-mediaservice.s3.eu-central-1.amazonaws.com/bensound-memories.mp3
* https://sst-mediaservice.s3.eu-central-1.amazonaws.com/bensound-ukulele.mp3


