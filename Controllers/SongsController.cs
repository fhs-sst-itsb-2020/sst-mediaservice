﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MediaStorageAPI.Data;
using MediaStorageAPI.Models;


namespace MediaStorageAPI.Controllers
{
    [Route("mediastorage/api/[controller]")]
    [ApiController]
    public class SongsController : ControllerBase
    {
        private readonly SongContext _context;

        public SongsController(SongContext context)
        {
            _context = context;
        }

        // GET: api/Songs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<int>>> GetSong()
        {
            var l = await _context.Song.ToListAsync();
            var ids = new List<int>();

            foreach(Song song in l)
            {
                ids.Add(song.Id);
            }

            return ids;
        }

        // GET: api/Songs/InsertDummySong
        [HttpGet("InsertDummySong")]
        public async Task<ActionResult<int>> InsertDummySong()
        {

            Song dummySong = new Song();
            dummySong.BlobData = System.IO.File.ReadAllBytes(@"ExampleData/nevergonnagiveyouup.mp3");
            _context.Song.Add(dummySong);
            await _context.SaveChangesAsync();

            return dummySong.Id;
        }

        // GET: api/Songs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SongBase64>> GetSong(int id)
        {
            var song = await _context.Song.FindAsync(id);

            if (song == null)
            {
                return NotFound();
            }

            var ConvertedSong = new SongBase64(song.Id, song.BlobData);
            return ConvertedSong;
        }

        // PUT: api/Songs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSong(int id, Song song)
        {
            if (id != song.Id)
            {
                return BadRequest();
            }

            _context.Entry(song).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SongExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Songs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Song>> PostSong(SongBase64 b64song)
        {
            Song song = new Song(b64song.id, b64song.BlobString);

            _context.Song.Add(song);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSong", new { id = song.Id }, song);
        }

        // DELETE: api/Songs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Song>> DeleteSong(int id)
        {
            var song = await _context.Song.FindAsync(id);
            if (song == null)
            {
                return NotFound();
            }

            _context.Song.Remove(song);
            await _context.SaveChangesAsync();

            return song;
        }

        private bool SongExists(int id)
        {
            return _context.Song.Any(e => e.Id == id);
        }
    }
}
