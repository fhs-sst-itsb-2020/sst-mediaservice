﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaStorageAPI.Models
{
    public class SongBase64
    {
        public int id { get; set; }
        public string BlobString { get; set; }

        public SongBase64() { }

        //Converter Constructor
        public SongBase64(int id, byte[] blob)
        {
            this.id = id;
            BlobString = Convert.ToBase64String(blob);
        }

        



    }
}
