﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediaStorageAPI.Models
{
    public class Song
    {
        public int Id{ get; set; }
        public byte[] BlobData { get; set; }

        public Song() { }

        //Converter Constructor
        public Song(int id, string Base64String)
        {
            this.Id = id;
            this.BlobData = Convert.FromBase64String(Base64String);
        }
    }
}
