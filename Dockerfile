#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

#FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
#WORKDIR /app
#EXPOSE 80
#EXPOSE 443

#FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
#WORKDIR /
#COPY ["MediaStorageAPI.csproj", "MediaStorageAPI"]

#RUN ls /home
#RUN ls /MediaStorageAPI
#RUN ls /MediaStorageAPI/MediaStorageAPI
#RUN dotnet restore "MediaStorageAPI/MediaStorageAPI.csproj"

#COPY . .
#WORKDIR MediaStorageAPI/

#RUN dotnet build "MediaStorageAPI.csproj" -c Release -o /app/build

#FROM build AS publish
#RUN dotnet publish "MediaStorageAPI.csproj" -c Release -o /app/publish

#FROM base AS final
#WORKDIR /app
#COPY --from=publish /app/publish .
#ENTRYPOINT ["dotnet", "MediaStorageAPI.dll"]

#----- new version

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
#EXPOSE 80
#EXPOSE 443
EXPOSE 8089

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
#WORKDIR /src
WORKDIR /MediaStorageAPI

RUN ls 
RUN ls /home
#RUN ls /src
#RUN ls /app

RUN ls /MediaStorageAPI

#COPY ["MediaStorageAPI/MediaStorageAPI.csproj", "MediaStorageAPI/"]
#COPY MediaStorageAPI/MediaStorageAPI.csproj MediaStorageAPI/
COPY $CI_PROJECT_DIR/MediaStorageAPI.csproj MediaStorageAPI/
RUN dotnet restore "MediaStorageAPI/MediaStorageAPI.csproj"
COPY $CI_PROJECT_DIR MediaStorageAPI/
#COPY . .
#WORKDIR "/src/MediaStorageAPI"
WORKDIR /MediaStorageAPI/MediaStorageAPI
RUN dotnet build "MediaStorageAPI.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "MediaStorageAPI.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "MediaStorageAPI.dll"]
